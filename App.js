import * as React from 'react';
import { View } from 'react-native';

import Navigation from './components/Navigation'
export default function App() {
  return (
    <View style={{flex:1, backgroundColor:"white"}}>
      <Navigation/>
    </View>
    
  );
}
