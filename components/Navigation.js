import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SimpleLogin from './SimpleLogin'
import PhoneLogin from './PhoneLogin'
import Registration from './Registration'
import Accueil from './Accueil'
const Stack = createStackNavigator();

function Navigation () {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Accueil"
          component={Accueil}
          options={{ headerShown: false }}

        />
        <Stack.Screen
          name="Registration"
          component={Registration}
          options={{ headerShown: false }}

        />
        <Stack.Screen
          name="SimpleLogin"
          component={SimpleLogin}
          options={{ headerShown: false }}

        />
        <Stack.Screen name="PhoneLogin" component={PhoneLogin} 
          options={{ headerShown: false }}/>

      </Stack.Navigator>
      
    </NavigationContainer>
  );
}
export default Navigation;
