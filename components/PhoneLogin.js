import * as React from 'react';
import { View, Text, Image, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
export default function PhoneLogin({navigation}) {
  return (
    <View style={styles.home}>
      <Image
        style={{ height: 60, width: 70, padding:20, marginTop:70 }}
        source={require('../assets/dp.png')}></Image>
      <Text style={{ margin: 20, fontSize: 30, fontWeight: 'bold', marginBottom:50 }}>
        Connectez-vous
      </Text>

      <TextInput
        placeholder="Entrer votre nméro de éléphone"
        keyboardType="numeric"
        style={styles.input}
      />
      <TouchableOpacity
        style={{
          backgroundColor: '#11BD9E',
          minWidth: 300,
          height: 57,
          marginBottom: 20,
          borderRadius: 20,
        }}>
        <Text
          style={{
            color: 'white',
            textAlign: 'center',
            margin: 'auto',
            top: '30%',
            fontWeight: 'bold',
          }}>
          {' '}
          Se Connecter
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={{
          borderWidth: 1,
          borderColor: '#11BD9E',
          minWidth: 300,
          height: 57,
          borderRadius: 20,
        }}
        onPress = {()=>{navigation.navigate("SimpleLogin")}}
        >
        <Text
          style={{
            color: 'black',
            textAlign: 'center',
            margin: 'auto',
            top: '30%',
            fontWeight: 'bold',
          }}>
          
          Se Connecter avec une adresse email
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={{
          flexDirection:'row',
          marginTop:10,
          minWidth: 220,
          borderRadius:20,
          height: 57,
        }}
        onPress = {()=>{navigation.navigate("Registration")}}
        >
        
        <Text>Vous n'avez pas de compte </Text>
        <Text style={{fontWeight: "bold"}}>'s'inscrire'</Text>
      </TouchableOpacity>
    </View>
  );
}
const styles = StyleSheet.create({
home: {
        flex: 1,
        alignContent: 'center',
        alignItems: 'center',
        marginVertical: 50,
      },
input : {
          borderWidth: 1,
          borderRadius: 20,
          borderColor: '#F1F1F1',
          backgroundColor: '#FFFFFF',
          padding:10,
          color: '#5A5A5A',
          minWidth: 300,
          minHeight: 57,
          marginBottom: 30,
        }
})
