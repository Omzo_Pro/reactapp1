import * as React from 'react';
import { View, Text, Image, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
export default function Registration({navigation}) {
  return (
    
    <View style={styles.home}>
      <Image
        style={{ height: 60, width: 70, marginTop:50 }}
        source={require('../assets/dp.png')}></Image>
      <Text style={{ margin: 20, fontSize: 30, fontWeight: 'bold', marginBottom:20 }}>
        Inscrivez-vous
      </Text>

      <TextInput
        placeholder="Prénoms et Noms"
        style={styles.input}
      />
      <TextInput
        placeholder="Adresse mail"
        style={styles.input}
      />
      <TextInput
        placeholder="Mot de passe"
        style={styles.input}
      />
      <TextInput
        placeholder="Confirmer votre mot de passe"
        style={styles.input}
      />
      <TouchableOpacity
        style={{
          backgroundColor: '#11BD9E',
          minWidth: 220,
          borderRadius:20,
          height: 57,
        }}>
        <Text
          style={{
            color: 'white',
            textAlign: 'center',
            margin: 'auto',
            top: '30%',
            fontWeight: 'bold',
          }}>
          {' '}
          S'inscrire
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={{
          flexDirection:'row',
          marginTop:10,
          minWidth: 220,
          borderRadius:20,
          height: 57,
        }}
        onPress = {()=>{navigation.navigate("SimpleLogin")}}
        >
        
        <Text>Déja inscrit</Text>
        <Text style={{fontWeight: "bold"}}>'se connecter'</Text>
      </TouchableOpacity>
      
      
    </View>
  );
}
const styles = StyleSheet.create({
home: {
        flex: 1,
        alignContent: 'center',
        alignItems: 'center',
        marginVertical: 50,
      },
input : {
          borderWidth: 1,
          borderRadius: 20,
          borderColor: '#F1F1F1',
          backgroundColor: '#FFFFFF',
          padding:10,
          color: '#5A5A5A',
          minWidth: 220,
          minHeight: 57,
          marginBottom: 10,
        }
})
