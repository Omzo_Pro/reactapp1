import * as React from 'react';
import { View, Text, Image, TextInput, TouchableOpacity, StyleSheet, ImageBackground } from 'react-native';
export default function Accueil({navigation}) {
  return (
  <View style={styles.home}>
    <ImageBackground source={require('../assets/images/pro.jpg')} style={styles.image}>
      <View style={styles.view1}>
        <Image
          style={styles.image2}
          source={require('../assets/dp.png')}>
        </Image>
        <Text style={styles.text}>LANCEZ VOTRE CARRIERE MAINTENANT !</Text>
        
      </View>
    </ImageBackground>
    <View style={styles.view2}>
          <TouchableOpacity
            style={styles.touchable}
            onPress = {()=>{navigation.navigate("SimpleLogin")}}
            >
            <Text
              style={{

                color: 'black',
                textAlign: 'center',
                margin: 'auto',
                top: '30%',
                fontWeight: 'bold',
              }}>
              {' '}
              Connexion
            </Text>
          </TouchableOpacity>
          <TouchableOpacity 
            style={styles.touchable2}
            onPress = {()=>{navigation.navigate("Registration")}}
            >
            <Text
              style={{
                
                color: 'white',
                textAlign: 'center',
                margin: 'auto',
                top: '30%',
                fontWeight: 'bold',
              }}>
              {' '}
              Isnscriprion
            </Text>
          </TouchableOpacity>
        </View>
  </View>
  );
}

const styles = StyleSheet.create({
  
  home: {
    flex: 1,
    flexDirection: "column",
    alignContent: 'center',
    alignItems: 'center',
    marginVertical: 50,
  },
  image: {
    flex: 4,
    resizeMode: "contain",
    justifyContent: "center",
    width:250,

  },
  view1:{
    opacity: 50,    
    width:250,
    paddingTop:10,
    height:'100%',
    backgroundColor:'#11BD9E, 100 %'
  },
  view2:{
    flex: 1,
    flexDirection: "row",
    backgroundColor:'#11BD9E',
    width:250
  },
  image2:{
    resizeMode: "contain",
    height: 50,
    width: 60,
    padding:15,
    marginTop:40,
    marginLeft:10
  },
  text: {
    marginTop:30,
    fontFamily: 'DM Sans',
    lineHeight: 40,
    flex:6,
    color: "white",
    fontSize: 25,
    fontWeight: "bold",
    marginLeft:20,
    paddingTop:80,
    width:190
  },
  touchable:{
    flex:1,
    flexDirection:"row",
    backgroundColor: '#ffffff',
    borderRadius:20,
    height: 57,
    margin: 10
  },
  touchable2:{
    flex:1,
    flexDirection:"row",
    borderColor: '#ffffff',
    borderWidth:2,
    borderRadius:20,
    height: 57,
    margin: 10
  }
})
  